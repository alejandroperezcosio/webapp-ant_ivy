package org.timoponce.webapp;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;

@WebServlet(value="/hello", name="hello-servlet")
public class HelloServlet extends GenericServlet{
	
	public void service(ServletRequest req, ServletResponse res)
	    throws IOException, ServletException{
	    res.getWriter().println("Hello World!");
	}
	
}
