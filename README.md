Simple Web application template project 
======================================

Built using: 
	
- Java 
- Servlet 3 as implementation API
- Apache Ant for build tasks
- Apache Ivy for dependencies management
- Groovy for unit testing 
- Jacoco for test coverage 
